import Home from './components/Home';
import ListWrapper from './components/ListWrapper';
import Contacts from './components/Contacts';
import About from './components/About';
import Notfound from './components/Notfound';

export default [
    {
        path: '/',
        exact: true,
        component: Home
    },
    {
        path: '/list',
        
        component: ListWrapper
    },
    {
        path: '/contacts',
        
        component: Contacts
    },
    {
        path: '/about',
        
        component: About,
    },
    {
        component: Notfound
    }
]