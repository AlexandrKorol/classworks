import React from 'react';
import { BrowserRouter, Route, Switch, Link } from 'react-router-dom'; //Подключение
import './App.css';
import rootRoutes from './rootRoutes';
function App() {
  return (
    <BrowserRouter>
    <div>
      <header>
      <ul className="navbar">
        <li><Link to="/">Home Page</Link></li>
        <li><Link to="/list">List</Link></li>
        <li><Link to="/about">About</Link></li>
        <li><Link to="/contacts">Contacts</Link></li>
      </ul>
      </header>
      <Switch>
      {
        rootRoutes.map((route,key) =>(
          <Route key={key}
            {...route}
          />
        ))
      }
      </Switch>
    </div>
    </BrowserRouter>
  );
}

export default App;
