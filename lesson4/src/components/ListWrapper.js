import React from 'react';
import { Switch, Route } from 'react-router-dom';

import List from './List';
import ListItem from './Item';

const ListWrapper = () => (
    <Switch>
        <Route exact path="/list/:id" component={ListItem} />
        <Route  path="/list" component={List} />
        
    </Switch>
)


export default ListWrapper;