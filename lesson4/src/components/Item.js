import React from 'react';
import batman from '../assets/icons8-batman-emoji-33.png';

class Item extends React.Component {
  state = {
    list: {}
  }
  componentDidMount() {
    
    
    let listec = JSON.parse(window.localStorage.getItem('listec'));
    if(listec !== null){
      let result;
     listec.map(item=>{
      console.log('started process');
      if(item.id === parseInt(this.props.match.params.id))
       result = item;
      else{
       return null;
      }
    })
    this.setState({
      list: result
    })
    }
    
    else{
      fetch(`https://jsonplaceholder.typicode.com/users/${this.props.match.params.id}`)
      .then(res => res.json())
      .then(res => {
        this.setState({
          list: res
        })
      }
      )
    }
  }
  componentDidUpdate() {
    console.log("This is state", this.state);
  }
  render() {
    const { list } = this.state;
    return (
      <div>
        <h2>User: {list.id} </h2>
        <p>Name: {list.name} </p>
        <p>Username: {list.username} </p>
        <p>Email: {list.email} </p>
        <p>Phone: {list.phone} </p>
        {
          list.rank ? 
          <p> <strong>Ranked as</strong> {list.rank} stars of 10 batmen <img src={batman} alt= "I'M BATMAN"></img> </p>:
          null
        }
      </div>
    )
  }
}
export default Item;