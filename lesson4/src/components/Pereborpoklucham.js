import React from 'react';
import { Link } from 'react-router-dom';

const Item = ({ item }) => {

  const keys = Object.keys( item );

  return (
    <li>
      <Link to={`/list/${item.id}`}>
      <ul> 
        {
          keys.map( key => 
            <li>
              {key} 
              {
                typeof( item[key] ) !== 'object' && item[key]
              }
            </li>
            )
        }
        </ul>
        {item.name}
      </Link>
    </li>
  );
}

class List extends React.Component {
  state = {
    list: []
  }

  componentDidMount() {
    fetch("https://jsonplaceholder.typicode.com/users")
      .then(res => res.json())
      .then(res => {
        console.log(res);
        this.setState({
          list: res
        })
      })
  }

  render() {
    const { list } = this.state;
    return (
      <div>
        <h1>
          List
        </h1> 
        <ul>
          {
            list.map(item => (
              <Item key={item.id} item={item} />
            ))
          }
        </ul>
      </div>
    )
  }
}
export default List;



