import React from 'react';
import { Link } from 'react-router-dom';

const Item = ({ item, onChangeHandler }) => {
  return (
    <li>
      <Link to={`/list/${item.id}`}>
        {item.name}

      </Link>
      {
        item.rank ?
          <strong>Rated as: {item.rank} </strong> :
          null
      }
      <select onChange={onChangeHandler(item.id)}>
        <option >1</option>
        <option >2</option>
        <option >3</option>
        <option >4</option>
        <option >5</option>
      </select>
    </li>
  );
}



class List extends React.Component {
  state = {
    list: []
  }

  componentDidMount() {

    let result = JSON.parse(window.localStorage.getItem('listec'));
    if (result !== null) {
      this.setState({
        list: result
      })
    }
    else {
      fetch("https://jsonplaceholder.typicode.com/users")
        .then(res => res.json())
        .then(res => {
          this.setState({
            list: res
          })
        })
    }


  }
  toLocalStorage = (result) => {
    console.log("This is result", this.state.list)
    window.localStorage.setItem('listec', JSON.stringify(result));
  }
  onChangeHandler = (id) => (e) => {
    const { list } = this.state;
    const result = list.map(item => {
      if (item.id === id) {
        return {
          ...item,
          rank: e.target.value
        }
      }
      else {
        return item;
      }

    })

    this.setState({
      list: result
    });
    this.toLocalStorage(result);
  }

  render() {
    const { list } = this.state;
    return (
      <div>
        <h1>
          List
        </h1>
        <ul>
          {
            list.map(item => (
              <Item key={item.id} item={item} onChangeHandler={this.onChangeHandler} />
            ))
          }
        </ul>
      </div>
    )
  }
}
export default List;