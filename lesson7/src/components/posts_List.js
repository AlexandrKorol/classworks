import React from 'react'
import { connect } from 'react-redux';

import { getPosts } from '../actions';

export class List extends React.Component{

    componentDidMount(){
        this.props.getPosts();
    }

    render = () => {
        const { loaded, data } = this.props.posts;
        return(
            <>
                <h1> Posts </h1>
                {
                    loaded ? 
                    <ul>
                        {
                            data.map( item => (
                                <li key={item.id}>{ item.title} </li>
                            ))
                        }
                    </ul> :
                    <span>
                        Awaiting...
                    </span>
                }
            </>
        )
    }
}

const mapStateToProps = (state) => ({
    posts: state
});

const mapDispatchToProps = ( dispatch ) => ({
    getPosts: () => {
        dispatch( getPosts() );
    }
})



export default connect(mapStateToProps, mapDispatchToProps )(List);
