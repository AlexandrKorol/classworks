import React from 'react';
import { Switch, Route } from 'react-router-dom';

import List from './posts_List';
import ListItem from './Item';

const ListWrapper = () => (
    
    <Switch>
        <Route exact path="/posts" component={List} />
        <Route exact path="/posts/:id" component={ListItem}/>
    </Switch>
)


export default ListWrapper;