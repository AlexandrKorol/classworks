import {
    GET_POSTS_REQ,
    GET_POSTS_RES
} from '../actions';

const postsInitialState = {
    loaded: false,
    errors: [],
    data: [],
};

const postsReducer = ( state = postsInitialState, action) => {
    switch( action.type ){
        case GET_POSTS_REQ:
            return{
                ...state,
                loaded: false
            }
        case GET_POSTS_RES:
                return{
                    ...state,
                    loaded: true,
                    data: action.payload
                }
    
        default:
            return state;
    }
}

export default postsReducer;
