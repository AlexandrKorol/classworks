export const GET_POSTS_REQ = "GET_POSTS";
export const GET_POSTS_RES = 'GET_POSTS_RES';


export const getPost = ( post ) => ({
    type: GET_POSTS_REQ
});


export const getPosts = () => (dispatch) =>{
    dispatch(getPost());
    fetch('https://jsonplaceholder.typicode.com/posts/')
    .then(res=>res.json())
    .then(res=>{
        dispatch({
            type: GET_POSTS_RES,
            payload: res
        })
    })
}