import React from 'react';
import { BrowserRouter, Route, Switch, Link } from 'react-router-dom';
import { Provider } from 'react-redux'; //Подключение
import './App.css';
import store from './redux/store';
import rootRoutes from './rootRoutes';
import List from './components/posts_List';
function App() {
  return (

    <BrowserRouter>
      <Provider store={store}>
        <div>
          <header>
            <Link to="/">Home Page</Link>
          </header>
          <List/>
          <Switch>
            {
              rootRoutes.map((route, key) => (
                <Route key={key}
                  {...route}
                />
              ))
            }
          </Switch>
        </div>
      </Provider>
    </BrowserRouter >

  );
}

export default App;


// import React from 'react';
// import { Provider } from 'react-redux';
// // -- Lib

// import Page1 from './page/page1';
// import Page2 from './page/page2';
// import Posts from './posts';
// // -- Components

// // -- Helpers

// // -- Style images etc...
// import './App.css';

// import store from '../redux/store';

// const App = () => (
//   <Provider store={store}>
//       <Page1 />
//       <Page2 />
//       <Posts />
//   </Provider>
// );

// export default App;
