import Home from './components/Home';
import Wrapper from './components/Wrapper';
export default [
    {
        path: '/',
        component: Home,
        exact: true
    },
    {
        path: '/posts',
        component: Wrapper,
        
    }
]