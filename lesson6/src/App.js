import React from 'react';
import { BrowserRouter, Route, Switch, Link, NavLink, Redirect, Prompt, withRouter } from 'react-router-dom';
import './App.css';
import RootRoutes from './components/RootRoutes';

import PrivateRoute from './components/PrivateRoute';
import AuthContext from './components/Context';


class App extends React.Component {

  render(){
    return (
      <BrowserRouter>
        <AuthContext>
        <header>
          <ul>
            <li>
              <NavLink to="/login">Log in</NavLink>
            </li>
            <li>
              <NavLink to="/register">Register</NavLink>
            </li>
            <li>
              <NavLink to="/list">User List</NavLink>
            </li>
          </ul>
        </header>
        <Switch>
          {
            RootRoutes.map((route, index) => {
              
                if( route.private ){
                  return(
                    <PrivateRoute 
                      {...route}
                      key={index}
                    />
                  ) 
                } else {
                  return  (
                    <Route
                      {...route}
                      key={index}
                    />
                  )
                }
              })
          }
        </Switch>
        </AuthContext>
      </BrowserRouter>
    );
  }
}

export default App;
