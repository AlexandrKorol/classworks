import React from 'react';

export const UserContext = React.createContext();

export default class ContextComponent extends React.Component {

    state = {
        user: null
    }


    authUser = () => {

    }

    render() {
        const { children } = this.props;
        const { user } = this.state;
        return (
            <div>
                <UserContext.Provider 
                    value={{
                        test: '123',
                        user
                    }}>
                    {children}
                </UserContext.Provider>
            </div>
        )
    }
}
