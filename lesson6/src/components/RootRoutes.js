import React from 'react';

import Login from './Login';
import RegisterForm from './Register';

export default [
    {
        path:'/',
        exact: true,
        render: () => {
            return(
                <h1> Main page </h1>
            )
        }

    },

    {
        path: '/login',
        exact: true,
        component: Login
    },
    {
        path: '/register',
        exact: true,
        component: RegisterForm
    },
    {
        path: '/list',
        exact: true,
        component: () => (<h1>List</h1>),
        private: true
    }
]