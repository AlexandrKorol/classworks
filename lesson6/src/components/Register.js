import React from 'react';
// зробити классовий компонент
class Register extends React.Component{
    state = {
    user: {
        email: '',
        password: ''
    }
}

mailInput = (e) =>{
    this.setState({
        user:{
            email: e.target.value
        }
    })
}
passwordInput = (e) =>{
    this.setState({
        user:{
            password: e.target.value
        }
    })
}
clickHandler = (e) =>{
    e.preventDefault(); 
    console.log(this.state);
}
    render(){
        const {mailInput, passwordInput, clickHandler} = this;
        return(
            <div>
                <h1>Registration form</h1>
                <form>
                    <input placeholder="email" onChange={mailInput}/>
                    <input placeholder="password" type="password" onChange={passwordInput}/>
                    <button type="submit" onClick = {clickHandler}>Submit</button>
                </form>
            </div>
         )
    }
}


export default Register;

