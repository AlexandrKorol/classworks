import React from 'react'
import {
    Redirect,
    Route
} from 'react-router-dom';

import { UserContext } from './Context';

const PrivateRoute = ({ auth, location, ...props}) => {

    // if user null redirect на головну
    return(
        <UserContext.Consumer>
            {
                value => {
                    console.log('route:', value);
                    return <h1> Private Route </h1>
                }
            }
        </UserContext.Consumer>
    )
    // if( !auth ){
    //     return(
    //         <Redirect 
    //             to={{
    //                 pathname: "/login",
    //                 state: {
    //                     from: location.pathname
    //                 }
    //             }}
    //         />
    //     )
    // } else {
    //     return ( <Route {...props} /> );
    // }
}

export default PrivateRoute;