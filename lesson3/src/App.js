import React from 'react';
import './App.css';
import { Toggler, TogglerItem } from './Toggler';
import Input from './CustomInput';

class App extends React.Component {
  state = {
    //task1
    sides: 'left',
    top: 'active',
    //task2,3
    custominput: {
      task2Input:
      {
        name: 'task2Input',
        placeholder: 'Task2Input',
        title: 'Task2Input title',
        contentMaxLength: 4,
        value: ''
      },
      name: {
        name: 'Name',
        placeholder: 'Name',
        title: 'Some title',
        value: ''
      },
      password: {
        name: 'Password',
        placeholder: 'Password',
        type: 'password',
        title: 'Some title',
        contentMaxLength: 8,
        value: ''
      },
      age: {
        name: 'Age',
        placeholder: 'Age',
        title: 'Some title',
        value: ''
      },
      language: {
        name: 'Language',
        placeholder: 'Favourite language',
        title: 'Language',
      }
    },
    //task3
    formData: {
      layout: "left",
      gender: null,
      age: null,
    }
  }

  // changeStatus === action із завдання про тоглери
  changeStatus = (key, value) => (e) => {
    this.setState({
      [key]: value
    })

  }
  onChangeHandler = (key) => (e) => {
    this.setState({
      custominput: {
        ...this.state.custominput,
        [key]: {
          ...this.state.custominput[key],
          value: e.target.value,
          contentLength: e.target.value.length
        }
      }

    })
  }

  formRedaction = (key) => (e) => {

    this.setState({
      formData: {
        ...this.state.formData,
        [key]: e.target.value
      }
    })
  }
  formTogglerHandler = (key, value) => (e) => {
    this.setState({
      formData: {
        ...this.state.formData,
        [key]: value
      }
    })
  }
  sendForm = (e) => {
    const { gender, layout, } = this.state.formData;
    const { name, password, language, age, } = this.state.custominput;
    e.preventDefault();
    console.log("User's name: ", name.value);
    console.log("User's password: ", password.value);
    console.log("User's gender: ", gender);
    console.log("User's age: ", age.value);
    console.log("User's layout: ", layout);
    console.log("User's language: ", language.value);
    // - Имя
    // - Пароль
    // - Пол (переключалка)
    // - Возраст
    // - Layout (переключалка)
    // - Любимый язык

  }
  render() {
    const { sides, top, } = this.state;
    const { changeStatus, onChangeHandler, sendForm, formRedaction } = this;
    const { gender, layout } = this.state.formData;
    return (
      <div className="App">


        {/* Task1 */}
        <h2>Task 1</h2>
        <Toggler
          activeToggler={sides}
          changeStatus={changeStatus}
          customkey="sides"
        >
          <TogglerItem value="left" name="left" />
          <TogglerItem value="middle" name="center" />
          <TogglerItem value="right" name="right" />
          <TogglerItem value="baseline" name="baseline" />
        </Toggler>
        <Toggler
          activeToggler={top}
          customkey='top'
          changeStatus={changeStatus}
        >
          <TogglerItem value="active" name="male" />
          <TogglerItem value="inactive" name="female" />
        </Toggler>

        {/* Task2 */}
        <h2>Task 2</h2>
        {/* <Input type={type} placeholder={placeholder} title={title} value={value} contentMaxLength={contentMaxLength}
          contentLength={contentLength} onChangeHandler={onChangeHandler} /> */}
        <Input {...this.state.custominput.task2Input} onChangeHandler={onChangeHandler('task2Input')} />
        {/* Task3 */}
        <h2>Task 3</h2>
        <form>
          <Input {...this.state.custominput.name} onChangeHandler={onChangeHandler('name')} />
          <Input {...this.state.custominput.password} onChangeHandler={onChangeHandler('password')} />
          <p>Gender:</p>
          <Toggler
            activeToggler={gender}
            customkey='gender'
            changeStatus={this.formTogglerHandler}
          >
            <TogglerItem value="male" name="male" />
            <TogglerItem value="female" name="female" />
          </Toggler>
          <Input {...this.state.custominput.age} onChangeHandler={onChangeHandler('age')} />
          <p>Layout:</p>
          <Toggler
            activeToggler={layout}
            customkey='layout'
            changeStatus={this.formTogglerHandler}
          >
            <TogglerItem value="left" name="left" />
            <TogglerItem value="right" name="right" />
          </Toggler>
          <Input {...this.state.custominput.language} onChangeHandler={onChangeHandler('language')} />
          <button onClick={sendForm}>Send data</button>
        </form>
      </div>
    );
  }
}

export default App;
