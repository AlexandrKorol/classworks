import React from 'react';
import PropTypes from 'prop-types';

const Input = (props) => {
    return (
        <label>
            <div className="bold">
                {props.name}
            </div>
            <input type={props.type} maxLength={props.contentMaxLength} placeholder={props.placeholder} value={props.value} title={props.title}
             onChange={props.onChangeHandler} />
            <span>{props.contentLength}</span>
        </label>
    )
}
Input.propTypes={
    type: PropTypes.oneOf(['text', 'password','file', 'data']).isRequired,
    placeholder: PropTypes.string,
    value: PropTypes.any,
    onChangeHandler: PropTypes.func.isRequired,
    contentLength: PropTypes.number,  //в завданні вказано, що має бути bool, але як кількість введених символів може бути bool, а не число?
    contentMaxLength: PropTypes.number
}
 Input.defaultProps={
    type: 'text',
    placeholder: '',
    title:'My Cutom Input',

}
export default Input;