import React from 'react';
import PropTypes from 'prop-types';


export  class Toggler extends React.Component{

  static propTypes = {
    children: PropTypes.array.isRequired,
    activeToggler: PropTypes.string.isRequired,
    changeStatus: PropTypes.func.isRequired,
    customkey: PropTypes.string.isRequired
  }

    render(){
        let {children, activeToggler, changeStatus, customkey } = this.props;
        return(
          <div>
            <div className="togglerContainer">
              {
                React.Children.map(
                  children, 
                  (childrenItem) => { 

                    
                    return React.cloneElement(childrenItem, {
                      ...childrenItem.props,
                      active: childrenItem.props.value === activeToggler ? true : false,
                      changeStatus: changeStatus,
                      customkey: customkey
                    })
                  }
                )
                }
            </div>
          </div>
        );
    

  }
}



export const TogglerItem =({name,value,active, changeStatus,customkey})=>{
    return(
        <div className ={
            active === true ?
            'togglerItem active' :
            'togglerItem'

        }
        onClick = {
            changeStatus !== undefined ?
            changeStatus(customkey,value):
            null
        }
        >
            {name}
        </div>
    )
}
TogglerItem.propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.string,
  active: PropTypes.bool.isRequired,
  changeStatus: PropTypes.func.isRequired,
  customkey: PropTypes.string.isRequired
}