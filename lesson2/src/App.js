import React from 'react';
import './App.css';
import Button from './components/button';
import ListElement from './components/ListElement';
class App extends React.Component {

  state = {
    user: [],
    buttonStyle: {
      background: "lightblue",
      color: 'yellow',
      fontSize: '20px',
      border: "2px solid lightblue",
      borderRadius: "20px",
      padding:"10px"
    }
  }
  task1Buttonhandler = () => () => {
    console.log("You have ticked me");
  }
  interFunc = (id) => () => {
    const { user } = this.state;
    const result = user.map(item => {
      if (item.user.guid === id) {
        return (
          {
            ...item,
            interviewed: !item.interviewed
          }
        )
      }
      else {
        return item;
      }
    });
    this.setState({
      user: result
    })
  }
  componentDidMount() {
    fetch('http://www.json-generator.com/api/json/get/bTZYVcPPVK?indent=2')
      .then(res => res.json())
      .then(res => {

        const userData = res.map(user => {
          return {
            user,
            interviewed: false //Додав в об'єкт юзера параметр 
          };
        });
        this.setState({
          user: userData //запис усіх юзерів
        })

      })

  }

  render() {
    const { user, buttonStyle } = this.state;
    return (
      <div className="App">
        <h3>Custom button</h3>
        <Button interwiew={this.task1Buttonhandler} style={buttonStyle} task1='true'>Click me</Button>
        <ul>
          {
            user.map(item => {
              return (
                <ListElement user={item} index={item.user.index}>
                  <Button interwiew={this.interFunc} id={item.user.guid} style={buttonStyle} />
                </ListElement>
              )
            })
          }
        </ul>
      </div>
    );
  }
}

export default App;
