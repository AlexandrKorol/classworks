import React from 'react';

const Element = (props) => {
    if(props.user.interviewed){
        return (
            <li key={props.index} className="green">
                <p>{props.user.user.about}</p>
                <p>{props.user.user.age} </p>
                {props.children}
            </li>
        )
    }
    else{
        return (
            <li key={props.index}>
                <p>{props.user.user.about}</p>
                <p>{props.user.user.age} </p>
                {props.children}
            </li>
        )
    }
}
export default Element;