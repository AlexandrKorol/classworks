import React, { Component } from 'react'
import { connect } from 'react-redux';
import './style.css';
class MyToDo extends React.Component {

    state = {
        newToDo: '',
        toDoList: []
    }

    changeHandler = (e) =>{
        this.setState({
            newToDo: e.target.value,
            })
    }

    addToDoItem = () => {
        const { addToDo } = this.props;
        addToDo(this.state.newToDo);
        
    }
    componentDidUpdate(){
        const {removeToDo} = this.props;
        // removeToDo(this.props.toDoList);
    }

    completeTask = (e) =>{
        const {toDoList} = this.props;
         const todoforfunc = toDoList;
        console.log(toDoList);
        this.props.completeToDo(todoforfunc);
    }

    render() {
        const {addToDoItem, changeHandler, completeTask} = this;
        const {newToDo} = this.state;
        const {toDoList} = this.props;
        return (
            <div>
                <input placeholder="Enter here what to do" onChange={changeHandler}
                    value={newToDo}/>
                <button onClick={addToDoItem}>Add</button>
                <ul>
                    {
                        toDoList.map((item,index)=>(
                            <li className={ item.done ? "green" : "red"} key={index}>{item.title} <button onClick={completeTask}>Complete</button> </li>
                        ))
                    }
                </ul>
            </div>
        )
    }


}

const mapStateToProps = (state, ownProps) => {
    return {
        toDoList: state.toDoItems
    }
};

const mapDispatchToProps = (dispatch, ownProps) => ({
    
    addToDo: (anyNewToDo) => {
        console.log("This is own props");
        dispatch({
            type: 'ADD_TODO',
            payload: { title: anyNewToDo, done: false,  }
        })
    },
    // removeToDo: (toDoList) =>{
    //     dispatch({

    //     })
       
    // },
    completeToDo: (toDoList)=>{
        dispatch({
            type: 'COMPLETE_TODO',
            
        }
        )
    }
});


export default connect(mapStateToProps, mapDispatchToProps)(MyToDo);