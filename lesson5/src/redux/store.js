import { createStore, compose } from 'redux';

import reducres from '../reducers/reducers';

const composeEnhancers = process.env.NODE_ENV !== 'production' && typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__  ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : compose;

const store = createStore( reducres,  composeEnhancers() );

export default store;
