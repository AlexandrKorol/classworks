import React from 'react';

export const StorageContext = React.createContext();

class Storage extends React.Component{
    state={
        something: ''
    }
    render(){
        console.log("Storage props", this.props);
        const { children } = this.props;
        return(
            <StorageContext.Provider
                data={{
                    title: 'Alexandr app',
                    meta:'Something'
                }}>
                {children}
            </StorageContext.Provider>
        )
    }
}

export default Storage;