import React from 'react';
import ContextWrapper from './contextWrapper';
import HelmetHOC from './HelmetHOC';
import HelmetChanger from './HelmetChanger';


const MyHelmet = HelmetHOC({ title: "Sasha App!", description: '12345' })(HelmetChanger);

function App() {
  return (
    <div className="App">
      <ContextWrapper>
        <MyHelmet />
      </ContextWrapper>
    </div>
  );
}

export default App;
