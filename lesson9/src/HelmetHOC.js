import React from 'react';
import { StorageContext } from './contextWrapper';
import Helmet from 'react-helmet';

const headProvider = (params) => (Component) => {
    console.log("Head provider params", params);
    const withHeader = () => (
        <>
            <Helmet>
                <title>{params.title}</title>
                <description>{params.description}</description>
            </Helmet>
            <Component />
        </>

    )
    return withHeader;  
}

export default headProvider;