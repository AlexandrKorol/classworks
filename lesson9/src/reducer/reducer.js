const postsInitialState = {
    dogs: []
};

const postsReducer = ( state = postsInitialState, action) => {
    switch( action.type ){
        case 'ADD_DOG':
            return{
                ...state,
                dogs: [...dogs, action.payload]
            }

        default:
            return state;
    }
}

export default postsReducer;
